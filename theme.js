jQuery(document).ready(function($){
  //barra de podcast home
  function next(){
    nextPage = $(".next_page").attr("href");
    $.get(nextPage, function(data) {
      $("#article-header").remove();
      $("#article-actions").remove();
      $( ".blog-posts" ).html( data );
    });
  }
  function previous(){
    previousPage = $(".previous_page").attr("href");
    $.get(previousPage, function(data) {
      $("#article-header").remove();
      $("#article-actions").remove();
      $( ".blog-posts" ).html( data );
    });
  }

  $(function(){
    //inserçao de barra de usuario no topo
    $("div #theme-header").prepend('<div id="bar-user"></div>');
    $("#bar-user").prepend(document.getElementById("user"));
    $("#user").css("display", "inline-block");

    /*adiçao de link backoffice*/
    $(".ctrl-panel").after(document.getElementById("back"));
    $("#back").css("display", "inline-block");
    $("#article").append('<a href="javascript:next()"><i class="fa fa-angle-right" aria-hidden="true" id="next"></i></a>');
    $("#article").append('<a href="javascript:previous()"><i class="fa fa-angle-left" aria-hidden="true" id="previous"></i>');
  });
});
